FROM rust:1.60 as builder

WORKDIR /usr/src/myapp
COPY hello-world .

RUN cargo install --path .
CMD ["hello-world"]

FROM debian
RUN apt-get update && apt-get install && apt install -y procps && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/hello-world /usr/local/bin/hello-world
EXPOSE 8080
USER nobody
CMD ["hello-world"]